GENERAL INFORMATION
-------------------

The central government of the Netherlands has a standard Visual Identity that is used on every website. The so-called" "Rijkshuisstijl" (central government visual branding) has been translated into this Rijkshuisstijl theme. Reference to brand identity: https://www.rijkshuisstijl.nl. Drupal theme maintainers are not related to this site and use it only as reference.

FEATURES
--------

# Configurable for every government agency wordmark
# Configurable for the preset colour schemes
# Responsive design, mobile first
# Webrichtlijnen version 2 compliant
# Bootstrap v4 is used as design framework.
# A living style guide is rendered wit KSS Node.

COPYRIGHT AND LEGAL USE
-----------------------

Please note that the visual government branding itself is only legally allowed on official websites of the government of the Netherlands.
Unless stated otherwise, CC0 applies to the images of the Rijksoverheid used in the Rijkshuisstijl theme and documentation.

INSTALLATION
------------

- Install the 'Components libraries' module.
- Install the Bootstrap Rijkshuisstijl theme in `themes` directory.
- Enable the Bootstrap Rijkshuissijl theme.

I WANT TO EXTEND/DEVELOP/COMPILE
--------------------------------

Good to see! You want to explore the code of this theme! <3
If you want to run everything smoothly, we have to install a few things before we can start.

To start you need to install the following tools on your machine:
# GIT
# Node.js ( & NPM) >v6.x
# Grunt >v1.0
# Ruby & SASS

GIT
===
If you don't have GIT installed, download and install it from https://git-scm.com/downloads.

Node.js
=======
If you don't have Node.js installed, download and install it from https://nodejs.org/en/download/. After this, we can use NPM to install packages. You will need version 6 or higher of Node.JS, test with "node -v" in your terminal.

Grunt
=====
Install grunt globally:
In order to get started, you'll want to install Grunt's command line interface (CLI) globally. You may need to use sudo (for OSX, *nix, BSD etc) or run your command shell as Administrator (for Windows) to do this.

  $ npm install -g grunt-cli

Ruby & SASS
===========
To compile SASS with a grunt task, it requires you to have Ruby (http://www.ruby-lang.org/en/downloads/) and Sass (http://sass-lang.com/download.html) installed. If you're on OS X or Linux you probably already have Ruby installed; test with "ruby -v" in your terminal. When you've confirmed you have Ruby installed, run "gem install sass" to install Sass.

RUNNING THE WORKFLOW AND TASKS WITH GRUNT
-----------------------------------------

After installing all tools, go to the theme ROOT in your terminal. From there we can install all node modules and dependencies.

To install all dependencies:
============================
You have to do this only once for instalation. Do an "update" if the theme's dependencies are updated.

  $ npm install (If you get access error's run the command with sudo.)

To run all Grunt tasks use:
===========================

  $ grunt serve
