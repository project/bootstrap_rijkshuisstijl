jQuery(document).ready(function($) {
  // Load Tooltip.
  $("[data-toggle='tooltip']").tooltip({});

  // Load Focuspoint.
  if ($.isFunction($.fn.focusPoint)) {
    $(".load-focuspoint .focuspoint").focusPoint();
  }

  // Load Carousel.
  if ($.isFunction($.fn.slick)) {
    $(".carousel").slick({
      dots: true,
      accesibility: true,
      autoplay: true,
      fade: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 400,
      zIndex: 4,
      arrows: false,
      cssEase: "linear",
      waitForAnimate: false,
      pauseOnDotsHover: true,
      customPaging: function(slider, i) {
        return $(".carousel-slide:nth-child(" + (i + 1) + ") .slide-nav-link");
      },
    });
  }

  $(".carousel .slick-active .slide-content").show();

  // Fix carousel with focuspoint.
  $(".carousel .focuspoint").each(function() {
    var imgheight = $(this).find("img").attr("height");
    $(this).height(imgheight);
  });

  // Set pause / play button at slideshow.
  $(".carousel.slick-dotted").prepend("<button class='carouselControl' type='button'>" + Drupal.t("Pause") + "</button>");

  $(".carousel .carouselControl").on("click", function() {
    var pauseBtn = $(this);
    if (pauseBtn.hasClass("paused")) {
      $(".carousel").slick("slickPlay");
      pauseBtn.removeClass("paused");
      pauseBtn.text(Drupal.t("Pause"));
    } else {
      $(".carousel").slick("slickPause");
      pauseBtn.addClass("paused");
      pauseBtn.text(Drupal.t("Play"));
    }
  });

  // Make slides accessible with tabbing.
  $(".carousel .slick-dots li").on("mouseenter tab", function() {
    var activeSlide = $(this).index();
    var currentSlide = $(".carousel").slick("slickCurrentSlide");
    if (currentSlide !== activeSlide) {
      $(".carousel").slick("slickGoTo", parseInt(activeSlide), false);
      $(".slide-content").hide();
      $(this).find(".slide-content").show(400);
    } else {
      $(".slide-content").hide();
    }
  });

  // Adds accessibility aria text for navbar collapse links/buttons.
  // This event is fired when a collapse element has been hidden from the user (will wait for CSS transitions to complete).
  $("#navbarResponsive").on("hidden.bs.collapse", function() {
    $(this).closest(".navbar-branded").find(".navbar-toggler.icon-menu .sr-only").text(Drupal.t("Collapsed"));
    $(this).closest(".navbar-branded").find(".navbar-toggler.icon-zoek .sr-only").text(Drupal.t("Search & menu collapsed"));
  });

  // This event is fired when a collapse element has been made visible to the user (will wait for CSS transitions to complete).
  $("#navbarResponsive").on("shown.bs.collapse", function() {
    $(this).closest(".navbar-branded").find(".navbar-toggler.icon-menu .sr-only").text(Drupal.t("Expanded"));
    $(this).closest(".navbar-branded").find(".navbar-toggler.icon-zoek .sr-only").text(Drupal.t("Close search & menu"));
  });
  
  // Headerimage check if image is width or tall compared to its parent
  $('.header-media img').each(function(){
    var imgContainer = $(this).parent();
    
    var imgContainerP = (imgContainer.width()/imgContainer.height());
    
    var imgClass = (this.width/this.height > imgContainerP ) ? 'wide' : 'tall';
    $(this).addClass(imgClass);
  });
  

});
