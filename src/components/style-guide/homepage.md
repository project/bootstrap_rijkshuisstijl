# Living style guide for the `Bootstrap Rijkshuisstijl` Theme

This style guide documents the designs of reusable components that are used in this theme and website. These components are built with Sass variables, functions and mixins. To ensure it is always up-to-date, this style guide is automatically generated from comments in the Sass files and the Drupal template uses the twig templates from its components.

The structure and layout of the components is based on the principle of Atomic Design by Brad Frost. This is reflected in the organization of the directory structure and the format of the living style guide.

## Structure of patterns and components.

Bootstrap v4 is used as design framework and foundation for the development and design of all components.
Some components are expanded with features



## Structure of theme

### Our working directory: src


<dl>
<dt>**Defaults**</dt>
<dd>`components/base` — The default “base” components apply to HTML elements. Since all of the rulesets in this class of styles are HTML elements, the styles apply automatically.</dd>
<dt>**Layouts**</dt>
<dd>`components/layouts` — Layout components position major chunks of the page. They just apply positioning, no other styles.</dd>
<dt>**Components**</dt>
<dd>`components/components` — Miscellaneous components are grouped together, but feel free to further categorize these.</dd>
<dt>**Navigation**</dt>
<dd>`components/navigation` — Navigation components are specialized design components that are applied to website navigation.</dd>
<dt>**Forms**</dt>
<dd>`components/forms` — Form components are specialized design components that are applied to forms or form elements.</dd>
</dl>

In addition to the components, our component library also contains these folders:

<dl>
<dt>**Colors and Sass**</dt>
<dd>`components/init` — This Sass documents the colors used throughout the site and various Sass variables, functions and mixins. It also initializes everything we need for all other Sass files: variables, 3rd-party libraries, custom mixins and custom functions.</dd>
<dt>**Style guide helper files**</dt>
<dd>`components/style-guide` — files needed to build this automated style guide; includes some CSS overrides for the default KSS style guide</dd>
<dt>**Generated files**</dt>
<dd>`components/asset-builds` — location of the generated CSS; don't alter these files</dd>
</dl>

### Our rendered output directory: dist


### Drupal directories & files.
