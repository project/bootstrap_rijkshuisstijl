jQuery(document).ready(function($) {

  $('html').addClass('js').removeClass('no-js');


  $('.carousel').slick({
    dots: true,
    accesibility: true,
    autoplay: true,
    fade: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 400,
    zIndex: 4,
    arrows: false,
    cssEase: 'linear',
    waitForAnimate: false,
    customPaging: function(slider, i) {
      return $('.carousel-slide:nth-child(' + (i + 1) + ') .slide-nav-link');
    },
  }).prepend('<button class="carouselControl" type="button"></button>');

  $('.slick-active .slide-content').show();

  $('.carouselControl').on('click', function() {
    var pauseBtn = $(this);
    if (pauseBtn.hasClass('paused')) {
      $(".carousel").slick('slickPlay');
      pauseBtn.removeClass('paused');
    } else {
      $(".carousel").slick('slickPause');
      pauseBtn.addClass('paused');
    }
  });

  $('.slick-dots li').on('mouseenter', function() {
    var activeSlide = $(this).index();
    var currentSlide = $('.carousel').slick('slickCurrentSlide');
    if ( currentSlide !== activeSlide) {
      $('.carousel').slick('slickGoTo', parseInt(activeSlide), false);
      $('.slide-content').hide();
      $(this).find('.slide-content').show(400);
    } else {
      $('.slide-content').hide();
    }
  });

  $('[data-toggle="tooltip"]').tooltip();

});
