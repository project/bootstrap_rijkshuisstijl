'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function(grunt) {

  const sass = require('node-sass');

  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times.
  require('time-grunt')(grunt);

  // Automatically load required grunt tasks.
  require('jit-grunt')(grunt);

  grunt.loadNpmTasks('grunt-postcss');

  // Configurable paths.
  var config = {
    app: 'src',
    dist: 'dist',
    styleguide: 'dist/style-guide'
  };

  // Define the configuration for all the tasks.
  grunt.initConfig({

    // Project settings
    config: config,

    // Watches files for changes and runs tasks based on the changed files.
    watch: {
      options: {
        // Options for the watch task go here
        interval: 1000
      },
      babel: {
        files: ['<%= config.app %>/js/{,*/}*.js'],
        tasks: ['babel:dist', 'eslint']
      },
      babelTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['babel:test', 'test:watch']
      },
      uglify: {
        files: ['<%= config.app %>/js/{,*/}*.js'],
        tasks: ['uglify:dist']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      sass: {
        files: ['<%= config.app %>/components/**/*.scss'],
        tasks: ['sass', 'postcss', 'kss']
      },
      twig: {
        files: ['<%= config.app %>/components/**/*.twig'],
        tasks: ['kss']
      },
      json: {
        files: ['<%= config.app %>/components/**/*.json'],
        tasks: ['kss']
      },
      kss_scripts: {
        files: ['<%= config.app %>/components/**/*.js'],
        tasks: ['kss']
      }
    },

    kss: {
      options: {
        title: 'Bootstrap Rijkhuisstijl Style Guide',
        'extend-drupal8': true,
        builder: '<%= config.app %>/components/style-guide',
        markup: true
      },
      dist: {
        src: ['<%= config.app %>'],
        dest: '<%= config.styleguide %>'
      }
    },

    browserSync: {
      options: {
        notify: false,
        background: true,
        watchOptions: {
          ignored: ''
        }
      },
      livereload: {
        options: {
          port: 9000,
          server: {
            baseDir: ['<%= config.styleguide %>'],
          }
        }
      },
      test: {
        options: {
          port: 9000,
          open: false,
          logLevel: 'silent',
          host: 'localhost',
          server: {
            baseDir: ['<%= config.styleguide %>', './test', config.app],
          }
        }
      },
      dist: {
        options: {
          background: false,
          server: '<%= config.styleguide %>'
        }
      }
    },

    // Empties folders to start fresh.
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      server: '<%= config.styleguide %>'
    },

    // Make sure code styles are up to par and there are no obvious mistakes.
    eslint: {
      target: [
        'Gruntfile.js',
        '<%= config.app %>/js/{,*/}*.js',
        '!<%= config.app %>/js/vendor/*',
        'test/spec/{,*/}*.js'
      ]
    },

    // Mocha testing framework configuration options.
    mocha: {
      all: {
        src: ['http://<%= browserSync.test.options.host %>:<%= browserSync.test.options.port %>/index.html'],
      },
      options: {
        run: true
      }
    },

    // Compiles ES6 with Babel.
    babel: {
      options: {
        sourceMap: true
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/js',
          src: '{,*/}*.js',
          dest: '<%= config.dist %>/js',
          ext: '.js'
        }]
      },
      test: {
        files: [{
          expand: true,
          cwd: 'test/spec',
          src: '{,*/}*.js',
          dest: '<%= config.dist %>/spec',
          ext: '.js'
        }]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested - using node-sass.
    //
    // All colors separately declared due to compile errors node-sass, if all files are in 1 files array.
    sass: {
      options: {
        implementation: sass,
        sourceMap: true,
        includePaths: ['scss'],
        precision: 6,
        sourceComments: false,
        outputStyle: 'expanded'
      },
      color_azure: {
        files: {
          '<%= config.dist %>/css/azure.css': '<%= config.app %>/components/schemes/azure.scss'
        }
      },
      color_brown: {
        files: {
          '<%= config.dist %>/css/brown.css': '<%= config.app %>/components/schemes/brown.scss'
        }
      },
      color_dark_blue: {
        files: {
          '<%= config.dist %>/css/dark-blue.css': '<%= config.app %>/components/schemes/dark-blue.scss'
        }
      },
      color_dark_brown: {
        files: {
          '<%= config.dist %>/css/dark-brown.css': '<%= config.app %>/components/schemes/dark-brown.scss'
        }
      },
      color_dark_green: {
        files: {
          '<%= config.dist %>/css/dark-green.css': '<%= config.app %>/components/schemes/dark-green.scss'
        }
      },
      color_dark_yellow: {
        files: {
          '<%= config.dist %>/css/dark-yellow.css': '<%= config.app %>/components/schemes/dark-yellow.scss'
        }
      },
      color_green: {
        files: {
          '<%= config.dist %>/css/green.css': '<%= config.app %>/components/schemes/green.scss'
        }
      },
      color_light_blue: {
        files: {
          '<%= config.dist %>/css/light-blue.css': '<%= config.app %>/components/schemes/light-blue.scss'
        }
      },
      color_mint_green: {
        files: {
          '<%= config.dist %>/css/mint-green.css': '<%= config.app %>/components/schemes/mint-green.scss'
        }
      },
      color_moss_green: {
        files: {
          '<%= config.dist %>/css/moss-green.css': '<%= config.app %>/components/schemes/moss-green.scss'
        }
      },
      color_orange: {
        files: {
          '<%= config.dist %>/css/orange.css': '<%= config.app %>/components/schemes/orange.scss'
        }
      },
      color_pink: {
        files: {
          '<%= config.dist %>/css/pink.css': '<%= config.app %>/components/schemes/pink.scss'
        }
      },
      color_purple: {
        files: {
          '<%= config.dist %>/css/purple.css': '<%= config.app %>/components/schemes/purple.scss'
        }
      },
      color_red: {
        files: {
          '<%= config.dist %>/css/red.css': '<%= config.app %>/components/schemes/red.scss'
        }
      },
      color_ruby: {
        files: {
          '<%= config.dist %>/css/ruby.css': '<%= config.app %>/components/schemes/ruby.scss'
        }
      },
      color_violet: {
        files: {
          '<%= config.dist %>/css/violet.css': '<%= config.app %>/components/schemes/violet.scss'
        }
      },
      color_yellow: {
        files: {
          '<%= config.dist %>/css/yellow.css': '<%= config.app %>/components/schemes/yellow.scss'
        }
      },
      color_white: {
        files: {
          '<%= config.dist %>/css/white.css': '<%= config.app %>/components/schemes/white.scss'
        }
      },
      styleguide: {
        files: {
          '<%= config.app %>/components/style-guide/kss-assets/css/kss.css': '<%= config.app %>/components/style-guide/kss-assets/kss.scss'
        },
      }
    },

    postcss: {
      options: {
        map: true,
        processors: [
          // Add vendor prefixed styles
          require('autoprefixer')({
            grid: true,
            browsers: [
              'Android 2.3',
              'Android >= 4',
              'Chrome >= 35',
              'Firefox >= 31',
              // Note: Edge versions in Autoprefixer & Can I Use refer to the EdgeHTML rendering engine version,
              // NOT the Edge app version shown in Edge's "About" screen.
              // For example, at the time of writing, Edge 20 on an up-to-date system uses EdgeHTML 12.
              // See also https://github.com/Fyrd/caniuse/issues/1928
              'Edge >= 12',
              'Explorer >= 9',
              'iOS >= 7',
              'Opera >= 12',
              'Safari >= 7.1'
            ]
          })
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.dist %>/css/',
          src: '{,*/}*.css',
          dest: '<%= config.dist %>/css/'
        }]
      }
    },

    // The following *-min tasks produce minified files in the dist folder.
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,*/}*.{gif,jpeg,jpg,png}',
          dest: '<%= config.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= config.dist %>/images'
        }]
      }
    },

    uglify: {
      dist: {
        files: {
          '<%= config.dist %>/js/main.js': [
            '<%= config.dist %>/js/main.js'
          ]
        }
      }
    },

    // Concat Javascript files.
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        files: {
          '<%= config.app %>/components/style-guide/kss-assets/js/npm.js': ['node_modules/jquery/dist/jquery.js', 'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', 'node_modules/slick-carousel/slick/slick.js', 'node_modules/jquery-focuspoint/js/jquery.focuspoint.js', 'node_modules/mocha/mocha.js', 'node_modules/chai/chai.js'],
          '<%= config.dist %>/js/bootstrap.js': ['node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'],
          '<%= config.dist %>/js/slick.js': ['node_modules/slick-carousel/slick/slick.min.js'],
          '<%= config.dist %>/js/focuspoint.js': ['node_modules/jquery-focuspoint/js/jquery.focuspoint.min.js'],
        },
      },
    },

    // Copies remaining files to places other tasks can use.
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '*.{ico,png,txt}',
            'images/{,*/}*.*',
            'fonts/{,*/}*.*'
          ]
        }]
      },
      kss: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.styleguide %>/kss-assets',
          src: [
            '*.{ico}',
            'fonts/{,*/}*.*'
          ]
        }]
      }
    },

    // Run some tasks in parallel to speed up build process.
    concurrent: {
      server: [
        'babel:dist',
        'sass'
      ],
      test: [
        'babel'
      ],
      dist: [
        'babel',
        'sass',
        'imagemin',
        'svgmin'
      ]
    },

    // Use auto install to do a npm update/install.
    auto_install: {
      local: {
        options: {
          npm: true
        }
      }
    }

  });

  grunt.registerTask('styleguide', [
    'kss'
  ]);

  grunt.registerTask('serve', 'start the server and preview your app', function(target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'browserSync:dist']);
    }

    grunt.task.run([
      'auto_install',
      'clean',
      'concurrent:server',
      'postcss',
      'kss',
      'copy',
      'concat',
      'uglify',
      'browserSync:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function(target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run([target ? ('serve:' + target) : 'serve']);
  });

  grunt.registerTask('test', function(target) {
    if (target !== 'watch') {
      grunt.task.run([
        'clean:server',
        'concurrent:test',
        'postcss'
      ]);
    }

    grunt.task.run([
      'browserSync:test',
      'mocha'
    ]);
  });

  grunt.registerTask('build', [
    'auto_install',
    'clean:dist',
    'concurrent:dist',
    'postcss',
    'concat',
    'uglify',
    'copy:dist',
    'kss'
  ]);

  grunt.registerTask('default', [
    'newer:eslint',
    'test',
    'build'
  ]);

};
